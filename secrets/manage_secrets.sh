#!/bin/bash

set -e
export MANIFEST_PATH='./secrets/manifests/'
export SCRIPTS_PATH='./secrets/scripts/'
export OUTPUT_FUNC="${SCRIPTS_PATH}/output_func.sh"
export JSON_DEF="${MANIFEST_PATH}/definitions.json"

export BLUE='\033[34m'
export YELLOW='\033[33m'
export RED='\033[91m'
export GREEN='\033[92m'
export NC='\033[39m'


function secret_already_exist() {
	kubectl -n $GIVEN_NAMESPACE get secrets $SECRET_NAME 2>&- >/dev/null
	return $?
}
export -f secret_already_exist

random_string () {
	head /dev/urandom | tr -dc A-Za-z0-9 | head -c 42
}
export -f random_string

# === START SCRIPTS EXECUTION ===

SECRET_SCRIPTS=("celery-krak-credentials.sh" "hermes-password.sh")

start_secret_generation() {
	echo -e "[${GREEN}+${NC}] Starting to execute ${YELLOW}$1${NC}"
	bash $SCRIPTS_PATH/$1
}

for script in ${SECRET_SCRIPTS[*]}; do
	start_secret_generation $script
done
