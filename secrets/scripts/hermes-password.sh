#!/bin/bash
# Randomly generate credentials for basic RabbitMQ users

set -e
source $OUTPUT_FUNC

export SECRET_NAME='hermes-setup-password'
MANIFEST_FILE="${MANIFEST_PATH}/hermes-password.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	# MANAGEMENT USER
	MANAGEMENT_PASSWORD=$(random_string)
	MANAGEMENT_PASSWORD_B64=$(base64 <<< $MANAGEMENT_PASSWORD | tr -d '\n')
	sed -i 's/HELM_VAR_MANAGEMENT_PASSWORD/'"$MANAGEMENT_PASSWORD"'/g' $JSON_DEF
	sed -i 's/HELM_VAR_MANAGEMENT_PASSWORD/'"$MANAGEMENT_PASSWORD_B64"'/g' $MANIFEST_FILE

	# GUEST USER
	GUEST_PASSWORD=$(random_string)
	GUEST_PASSWORD_B64=$(base64 <<< $GUEST_PASSWORD | tr -d '\n')
	sed -i 's/HELM_VAR_GUEST_PASSWORD/'"$GUEST_PASSWORD"'/g' $JSON_DEF
	sed -i 's/HELM_VAR_GUEST_PASSWORD/'"$GUEST_PASSWORD_B64"'/g' $MANIFEST_FILE

	# ERLANG
	ERLANG_COOKIE_PASSWORD_B64=$(random_string | base64 | tr -d '\n')
	sed -i 's/HELM_VAR_ERLANG_COOKIE_PASSWORD/'"$ERLANG_COOKIE_PASSWORD_B64"'/g' $MANIFEST_FILE

	# DEFINITION_JSON
	DEFINITION_JSON=$(base64 $JSON_DEF | tr -d '\n')
	sed -i 's/HELM_VAR_DEFINITION_JSON/'"$DEFINITION_JSON"'/g' $MANIFEST_FILE
#	rm -f $JSON_DEF

	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
#	rm -f $MANIFEST_FILE
fi
