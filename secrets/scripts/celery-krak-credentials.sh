#!/bin/bash
# Create rabbitMQ Uri secret for all celery apps

set -e
source $OUTPUT_FUNC

export SECRET_NAME="celery-krak-hermes-rabbitmq-credentials"
MANIFEST_FILE="${MANIFEST_PATH}/celery-krak-credentials.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	KRAK_PASSWORD=$(random_string)
	BASE_URL="amqp://celery-krak:${KRAK_PASSWORD}@hermes-rabbitmq-ha.${GIVEN_NAMESPACE}.svc.cluster.local"
	RABBITMQ_URL_B64=$(base64 <<< $BASE_URL | tr -d '\n')
	sed -i 's/HELM_VAR_CELERY_KRAK_PASSWORD/'"$KRAK_PASSWORD"'/g' $JSON_DEF
	sed -i 's/RABBITMQ_CREDENTIALS/'"$RABBITMQ_URL_B64"'/g' $MANIFEST_FILE

	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
#	rm -f $MANIFEST_FILE
fi
