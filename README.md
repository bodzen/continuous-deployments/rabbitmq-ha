# Hermes

Custom Helm Charts to install a Highly available RabbitMQ service.
This chart is based on the [official chart](https://github.com/helm/charts/tree/master/stable/rabbitmq-ha).


## How to

The namespace must be define in the variable **GIVEN_NAMESPACE**, before launching the pipeline.

As of now I can't make it work with existingSecrets.
Neither by creating a user.
So I just deploy rabbitmq-ha and manually set the value of celery-krak.. secret using guest user.

```
export GIVEN_NAMESPACE=omega
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install hermes --namespace ${GIVEN_NAMESPACE} bitnami/rabbitmq
export USER_PASSWORD="$(kubectl get secret --namespace ${GIVEN_NAMESPACE} hermes-rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)"
export URI_B64=$(base64 <<< "amqp://user:${USER_PASSWORD}@hermes-rabbitmq.${GIVEN_NAMESPACE}.svc.cluster.local
sed -i 's|RABBITMQ_CREDENTIALS|'"$URI_B64"'|g' secrets/manifests/celery-krak-credentials.yaml
kubectl -n $GIVEN_NAMESPACE apply -f secrets/manifests/celery-krak-credentials.yaml
```

## What it does

1. SetUp Helm if necessary (k8s accounts + init)
2. Generate password for the following users and store them as secrets
   - guest
   - management
   - celery-krak
3. Install the custom chart


```
** Please be patient while the chart is being deployed **

  Credentials:

    Username            : guest
    Password            : $(kubectl get secret --namespace default hermes-rabbitmq-00-rabbitmq-ha -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)
    Management username : management
    Management password : $(kubectl get secret --namespace default hermes-rabbitmq-00-rabbitmq-ha -o jsonpath="{.data.rabbitmq-management-password}" | base64 --decode)
    ErLang Cookie       : $(kubectl get secret --namespace default hermes-rabbitmq-00-rabbitmq-ha -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)

  RabbitMQ can be accessed within the cluster on port 5672 at hermes-rabbitmq-00-rabbitmq-ha.default.svc.cluster.local

  To access the cluster externally execute the following commands:

    export POD_NAME=$(kubectl get pods --namespace default -l "app=rabbitmq-ha" -o jsonpath="{.items[0].metadata.name}")
    kubectl port-forward $POD_NAME --namespace default 5672:5672 15672:15672

  To Access the RabbitMQ AMQP port:

    amqp://127.0.0.1:5672/

  To Access the RabbitMQ Management interface:

    URL : http://127.0.0.1:15672
```

## Network Policy

The [default policy](https://gitlab.com/bodzen/continuous-deployments/rabbitmq-ha/blob/master/policy/rabbitmq-policy.yaml) is to deny all **INGRESS** and **EGRESS** traffic.

To communicate with the rabbit-mq cluster, you must create a policy specific the application deployed.

### Example

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-new-app-hermes-policy
  namespace: GIVEN_NAMESPACE
spec:
  podSelector:
    matchLabels:
      app: rabbitmq-ha
  ingress:
    - from:
	  - podSelector:
	    matchLabels:
		  app: "basic-celery"
  policyTypes:
    - Ingress
```
